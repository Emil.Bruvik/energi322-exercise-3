#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 10:58:29 2024

@author: ech022
"""

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

plt.style.use('./custom_latex_style.mplstyle')
sns.set_style('whitegrid')

class Data:
    def __init__(self):
        self.m = 0.005097
        self.g = 981
        self.H0 = 0.0
        self.L = 2420.0
        self.XB = 2000.0
        self.EA = 16.5e6 * 0.5
        self.Iter = 200
        self.Tol = 1e-9
        self.InitialGuess_H = 100
        self.InitialGuess_V = 150

def Residue(DATA, X):
    mg = DATA.m * DATA.g
    EA = DATA.EA
    H = X[0]
    V = X[1]
    RES = np.zeros(2)
    RES[0] = H / mg * (np.arcsinh(V / H) - np.arcsinh((V - mg * DATA.L) / H)) + H * DATA.L / EA - DATA.XB
    RES[1] = H / mg * (-np.sqrt(1 + ((V - mg * DATA.L) / H)**2) + np.sqrt(1 + (V / H)**2)) + DATA.L / EA * (V - mg * DATA.L / 2) - DATA.H0
    return RES

def Jacobian(DATA, X):
    mg = DATA.m * DATA.g
    EA = DATA.EA
    H = X[0]
    V = X[1]
    JJ = np.zeros((2, 2))
    JJ[0, 0] = 1 / mg * (np.arcsinh(V / H) - np.arcsinh((-mg * DATA.L + V) / H)) + \
               H / mg * (-V / H**2 * (1 + 1 / H**2 * V**2)**(-0.5) + \
               (-mg * DATA.L + V) / H**2 * (1 + (-mg * DATA.L + V)**2 / H**2)**(-0.5)) + DATA.L / EA
    JJ[0, 1] = H / mg * (1 / H * (1 + 1 / H**2 * V**2)**(-0.5) - \
               1 / H * (1 + (-mg * DATA.L + V)**2 / H**2)**(-0.5))
    JJ[1, 0] = 1 / mg * (-np.sqrt(1 + (-mg * DATA.L + V)**2 / H**2) + \
               np.sqrt(1 + 1 / H**2 * V**2)) + H / mg * \
               ((1 + (-mg * DATA.L + V)**2 / H**2)**(-0.5) * \
               (-mg * DATA.L + V)**2 / H**3 - \
               (1 + 1 / H**2 * V**2)**(-0.5) / H**3 * V**2)
    JJ[1, 1] = H / mg * (-(-mg * DATA.L + V) / H**2 * (1 + (-mg * DATA.L + V)**2 / H**2)**(-0.5) + \
               V / H**2 * (1 + 1 / H**2 * V**2)**(-0.5)) + DATA.L / EA
    return JJ

# Main solver
DATA = Data()
Error = 1.0
XOLD = np.array([DATA.InitialGuess_H, DATA.InitialGuess_V])
k = 1

while Error > DATA.Tol and k < DATA.Iter:
    Res = Residue(DATA, XOLD)
    JJ = Jacobian(DATA, XOLD)
    DX = np.linalg.solve(JJ, -Res)
    Error = np.linalg.norm(DX, np.inf)
    XOLD = XOLD + DX
    k += 1

H, V = XOLD
L = DATA.L
mg = DATA.m * DATA.g
EA = DATA.EA
S = np.arange(0, DATA.L + 1, 1)
X = H / mg * (np.arcsinh((V - mg * (L - S)) / H) - np.arcsinh((V - mg * L) / H)) + H * S / EA
Z = H / mg * (np.sqrt(1 + ((V - mg * (L - S)) / H)**2) - np.sqrt(1 + ((V - mg * L) / H)**2)) + S / EA * (V - mg * L + 0.5 * mg * S)
SF = (np.log(V + np.sqrt(H**2 + V**2)) * H**2 + (2 * EA * L * mg) + np.sqrt(H**2 + V**2) * V) / (2 * EA * mg)
V_B = V
V_A = V_B - mg * L
Tension = np.sqrt(H**2 + (V_B - mg*(L-S))**2)

# Report
print("---------------------------------------------")
print("Results Report")
print("---------------------------------------------")
print(f"Horizontal force at the fair lead: {H:12.8f} [N]")
print(f"Vertical force at the fair lead: {V:12.8f} [N]")
print(f"Stretched length of the cable: {SF:12.8f}")
print(f"Error: {Error:14.12f}")
print(f"Number of iterations: {k}")

#Plot of X vs Z
plt.figure(figsize=(12,10))
plt.title("Final configuration of the cable")
plt.xlabel("X (cm)")
plt.ylabel("Z (cm)")
plt.plot(X, Z)
plt.savefig('elastic_config.pdf')

#Plot of tension T(s) as a function of the arc-length
plt.figure(figsize=(12,10))
plt.title("Tension T(s) vs arc-length")
plt.xlabel("S (cm)")
plt.ylabel("T(s)")
plt.plot(S, Tension)
plt.savefig('elastic_tension.pdf')

#Maximum sag: minimum displacement of z
Z_arr = np.array(Z)
max_sag = Z_arr.min()
print("The maximum sag is ", max_sag, "cm")