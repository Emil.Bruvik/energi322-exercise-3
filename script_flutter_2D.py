#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 11:10:02 2024

@author: ech022
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from functions_flutter import func, matrices
import seaborn as sns

plt.style.use('./custom_latex_style.mplstyle')
sns.set_style('whitegrid')
# Assuming the matrices() and func() functions have been defined previously in Python

# DATA
DATA = {
    'C': 18.288,  # Wing chord
    'kh': 9.9818e+3,  # Linear plunge spring stiffness
    'kt': 1.614834e+6,  # Linear pitching spring stiffness
    'm': 12879.8,  # Mass per unit length
    'Ip': 6.7005e+5,  # Wing mass moment of inertia
    'dh': 0,  # Plunge damping coefficient
    'dt': 0,  # Pitching damping coefficient
    'khc': 0,  # Cubic plunge spring stiffness
    'ktc': 1.5 * 1.614834e+6,  # Cubic pitching spring stiffness, adjusted from DATA['kt']
    'xs': 1.0,  # Elastic axis dimensionless distance from the leading edge
    'xa': 0,  # Distance between the elastic axis and mass center
    'U': 49.0,  # Free-stream velocity
    'Rhoinf': 1.225,  # Air density
}

# Dimensionless quantities
Wh = np.sqrt(DATA['kh'] / DATA['m'])
Wt = np.sqrt(DATA['kt'] / DATA['Ip'])

DATA.update({
    'Xi_h': DATA['dh'] / (2 * np.sqrt(DATA['m'] * DATA['kh'])),
    'Xi_a': DATA['dt'] / (2 * np.sqrt(DATA['Ip'] * DATA['kt'])),
    'mu': DATA['m'] / (np.pi * DATA['Rhoinf'] * (DATA['C'] / 2) ** 2),
    'ra': np.sqrt(DATA['Ip'] / (DATA['m'] * (DATA['C'] / 2) ** 2)),
    'ah': DATA['xs'] - 1,
    'Omb': Wh / Wt,
    'Beta': DATA['ktc'] / DATA['kt'],
    'Gamma': DATA['khc'] / DATA['kh'],
    'Uinf': DATA['U'] / (DATA['C'] / 2 * Wt),
    'Psi1': 0.165,
    'Psi2': 0.335,
    'Eps1': 0.0455,
    'Eps2': 0.3,
})

# NUM
NUM = {
    'DT': 0.01,  # Time step
    'TFinal': 20000,  # Simulation time
    'CI': [0, 1 * np.pi / 180, 0, 0],  # Initial conditions
}

# Solution procedure
Tspan = (0, NUM['TFinal'])
CInitial = NUM['CI'] + [0, 0, 0, 0]

Term = matrices(DATA)  # Assuming the matrices function is defined

def ode_system(t, Z):
    return func(t, Z, DATA, Term, NUM)  # Assuming the func function is defined

solution = solve_ivp(ode_system, Tspan, CInitial, method='RK45',
                     t_eval=np.arange(Tspan[0], Tspan[1]+NUM['DT'], NUM['DT']))

# RESPONSE
RESPONSE = {
    'Time': solution.t * DATA['C'] / 2 / DATA['U'], 
    'Plunge': solution.y[0] * DATA['C'] / 2,
    'Pitching': solution.y[1] * 180 / np.pi,
    'DPlunge': solution.y[2] * (DATA['C'] / 2) ** 2 / DATA['U'],
    'DPitching': solution.y[3] * 180 / np.pi * DATA['C'] / 2 / DATA['U'],
}
plunge_response = RESPONSE['Plunge']
time_series = RESPONSE['Time']

maxima_indices = np.argwhere(np.r_[True, plunge_response[1:] > plunge_response[:-1]] &
                             np.r_[plunge_response[:-1] > plunge_response[1:], True]).flatten()

time_periods = np.diff(time_series[maxima_indices])
average_time_period = np.mean(time_periods)

flutter_frequency = 1 / average_time_period

print(f"Flutter frequency: {flutter_frequency:.3f} Hz")

# Plotting
plt.figure(figsize=(12, 8))
plt.plot(RESPONSE['Time'], RESPONSE['Plunge'], color='b')
plt.axhline(0, color='k', linestyle='--')
plt.title('Plunge oscillation')
plt.xlabel('Time [s]')
plt.ylabel('Plunge amplitude [m]')

plt.savefig('plunge.pdf')

plt.figure(figsize=(12, 8))
plt.plot(RESPONSE['Time'], RESPONSE['Pitching'], color='b')
plt.axhline(0, color='k', linestyle='--')
plt.xlim(0, 1000)
plt.title('Pitching oscillation')
plt.xlabel('Time [s]')
plt.ylabel('Pitching amplitude [º]')

plt.savefig('pitch_49.pdf')

#Flatter frequency
#Measure distance between two peaks * 2*pi


#Limit cycle oscillaiton
#Super critical velocity: Higher than flutter velocity
#Diforcation diagram: h vs h* (h=Plunge, h*=DPlunge)
plt.figure(figsize=(12, 8))
plt.plot(RESPONSE['Plunge'], RESPONSE['DPlunge'], color='black')
plt.xlim(-10.0, 10.0)
plt.title('LCO for $U_{\infty}=50 m/s$')
plt.xlabel('h')
plt.ylabel('$\dot{h}$')

plt.savefig('LCO.pdf')

plt.figure(figsize=(12, 8))
plt.plot(RESPONSE['Pitching'], RESPONSE['DPitching'], color='black')
plt.xlim(-10.0, 10.0)
plt.title('LCO for $U_{\infty}=50 m/s$')
plt.xlabel(r'$\theta$')
plt.ylabel(r'$\dot{\theta}$')

plt.savefig('LCO_pitch.pdf')
#Bifurcation curve
#y:Amp(h) x:freestream velocity

plt.figure(figsize=(12, 8))

free_vel = np.linspace(10, 80, 70)
plunge_amplitudes = []
pitching_amplitudes = []

for Uinf in free_vel:
    DATA['U'] = Uinf
    DATA['Uinf'] = Uinf / (DATA['C'] / 2 * Wt)
    
    solution = solve_ivp(ode_system, Tspan, CInitial, method='RK45',
                         t_eval=np.arange(Tspan[0], Tspan[1]+NUM['DT'], NUM['DT']))
    
    plunge_response = solution.y[0] * DATA['C'] / 2
    pitching_response = solution.y[1] * 180 / np.pi
    
    steady_state_plunge = plunge_response[int(0.9 * len(plunge_response)):]  #Last 10% of the response
    steady_state_pitching = pitching_response[int(0.9 * len(pitching_response)):]
    
    max_plunge_amplitude = np.max(np.abs(steady_state_plunge))
    max_pitching_amplitude = np.max(np.abs(steady_state_pitching))
    
    plunge_amplitudes.append(max_plunge_amplitude)
    pitching_amplitudes.append(max_pitching_amplitude)

plt.figure(figsize=(12, 8))
plt.plot(free_vel, plunge_amplitudes, label='Plunge Amplitude', color='b')
plt.plot(free_vel, pitching_amplitudes, label='Pitching Amplitude', color='r')
plt.xlim(0, 90)
plt.title('Bifurcation Curve')
plt.xlabel('Free-stream Velocity $U_{\infty}$ [m/s]')
plt.ylabel('Amplitude [m] or [º]')
plt.legend()
plt.savefig('bifurcation_curve.pdf')



