#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 10:53:34 2024

@author: ech022
"""

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

plt.style.use('./custom_latex_style.mplstyle')
sns.set_style('whitegrid')

# Define a class to hold the data
class DATA:
    m = 0.005097  # Mass per unit length of the cable
    g = 981  # Gravity acceleration
    E = 16.5e6  # Young's modulus
    A = 0.5  # Cross sectional area
    EA = E * A
    H0 = 0  # Z-coordinate of the anchoring at X = 0 (negative for underwater applications)  
    L = 2420  # Length of the cable
    XB = 2000  # X-coordinate of the fair lead (or support) at X = XB
    Iter = 200  # Maximum number of iterations for the Newton-Raphson (N-R) method
    Tol = 1e-9  # Tolerance to stop the N-R method
    InitialGuess_H = 50  # Initial guess for the horizontal force H at X = XB
    InitialGuess_V = 55  # Initial guess for the vertical force V at X = XB

# Define the Residue function
def Residue(DATA, X):
    mg = DATA.m * DATA.g
    H = X[0]
    V = X[1]
    RES = np.zeros(2)
    RES[0] = H / mg * (np.arcsinh(V / H) - np.arcsinh((V - mg * DATA.L) / H)) - DATA.XB
    RES[1] = H / mg * (np.sqrt(1 + ((V - mg * DATA.L) / H)**2) - np.sqrt(1 + (V / H)**2)) - DATA.H0
    return RES

# Define the Jacobian function
def Jacobian(DATA, X):
    mg = DATA.m * DATA.g
    H = X[0]
    V = X[1]
    JJ = np.zeros((2, 2))
    JJ[0, 0] = 1 / mg * (np.arcsinh(V / H) - np.arcsinh((-mg * DATA.L + V) / H)) + \
               H / mg * (-V / H**2 * (1 + V**2 / H**2)**(-0.5) + \
               (-mg * DATA.L + V) / H**2 * (1 + (-mg * DATA.L + V)**2 / H**2)**(-0.5))
    JJ[0, 1] = H / mg * (1 / H * (1 + V**2 / H**2)**(-0.5) - \
               1 / H * (1 + (-mg * DATA.L + V)**2 / H**2)**(-0.5))
    JJ[1, 0] = 1 / mg * (np.sqrt(1 + (-mg * DATA.L + V)**2 / H**2) - np.sqrt(1 + V**2 / H**2)) + \
               H / mg * (-(1 + (-mg * DATA.L + V)**2 / H**2)**(-0.5) * \
               (-mg * DATA.L + V)**2 / H**3 + (1 + V**2 / H**2)**(-0.5) * V**2 / H**3)
    JJ[1, 1] = H / mg * (-V / H**2 * (1 + V**2 / H**2)**(-0.5) + \
               (-mg * DATA.L + V) / H**2 * (1 + (-mg * DATA.L + V)**2 / H**2)**(-0.5))
    return JJ

# Solver
Error = 1.0
XOLD = np.array([DATA.InitialGuess_H, DATA.InitialGuess_V])
k = 1

while Error > DATA.Tol and (k < DATA.Iter):
    Res = Residue(DATA, XOLD)
    JJ = Jacobian(DATA, XOLD)
    DX = np.linalg.solve(JJ, -Res)
    Error = np.linalg.norm(DX, np.inf)
    XOLD = XOLD + DX
    k += 1

# Report
print("---------------------------------------------")
print("Results Report")
print("---------------------------------------------")
print(f"Horizontal force at the fair lead: {XOLD[0]:12.8f} [N]")
print(f"Vertical force at the fair lead: {XOLD[1]:12.8f} [N]")
print(f"Error: {Error:14.12f}")
print(f"Number of iterations: {k}")
print('plot of x vs z')

H, V = XOLD
L = DATA.L
mg = DATA.m * DATA.g
EA = DATA.EA
S = np.arange(0, DATA.L + 1, 1)
X = H / mg * (np.arcsinh((V - mg * (L - S)) / H) - np.arcsinh((V - mg * L) / H)) + H * S / EA
Z = H / mg * (np.sqrt(1 + ((V - mg * (L - S)) / H)**2) - np.sqrt(1 + ((V - mg * L) / H)**2)) + S / EA * (V - mg * L + 0.5 * mg * S)
V_B = V
V_A = V_B - mg * L
Tension = np.sqrt(H**2 + (V_B - mg*(L-S))**2)

plt.figure(figsize=(12,10))
plt.title("Final configuration of the inelastic catenary")
plt.xlabel("X (cm)")
plt.ylabel("Z (cm)")
plt.plot(X, Z)
plt.savefig('inelastic_config.pdf')

plt.figure(figsize=(12,10))
plt.title("Tension T(s) vs arc-length")
plt.xlabel("S (cm)")
plt.ylabel("T(s)")
plt.plot(S, Tension)
plt.savefig('inelastic_tension.pdf')

#Maximum sag: minimum displacement of z
Z_arr = np.array(Z)
max_sag = Z_arr.min()
print("The maximum sag is ", max_sag, "cm")

